# Universal Grant Program

Python program that gives education grants to students based on financial information provided. The project is broken up into 4 major parts.  



## Version 4 - 

Part 4 will be designed using a main class called `StudentApp()` version 4.0 of the program will then but built functionally. As opposed to the previous versions, version 4 will have its functionalities broken up into functions. Each module will perform one function. The student data from each student will then be appended to a data structure within the program. From this data structure the other parts have the program will then gain access and print out the information for those students that were approved for a grant, shortlisted for approval and rejected. 



- `menu()` - This function will present the main menu to the end user. They'll be able to select a particular option and the be presented with the interface relating to said option.  The menu function will have a level of error-checking. If the user should enter some invalid then they'll be presented with an error message and be asked to re-enter a valid option. 

  The end user will be presented with: 

  ```tex
  ------------------------------
   UL GRANT APPLICATION SYSTEM
  ------------------------------
  
  A. Input Application Details for a Student.             
  B. Display Summary of Applications.             
  C. Display Grant Awardees.             
  X. Exit
  ```

  On entering an invalid option they'll be presented with the following message: 

  ```tex
  Error!! Only options A, B, C & X are valid. Please try again:
  ```

  

- `get_student_details()` - This function will allow the user to submit the applications for a set amount of students. It would then pass the information to the `make_instance()`.  The function should display the following input options when called, and allow the user to enter the details for one student.  

  ```tex
  ------------------------------
   Student Application Details
  ------------------------------
  Enter students full name: Andre Mckenzie
  Enter Student GPA: 3.4
  Enter tuition: 45600
  ```

  

- `make_instance()` - This function passes the students name, GPA and shortfall and pass them to the `StuddentApp()` class and build an instance for one particular student. 

  