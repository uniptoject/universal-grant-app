
import subprocess as sp
import json

# from student_app import StudentApp

student_data={}

def menu():
    """Displays a set of menu options for the user to select from
    """
    sp.call('clear', shell=True)
    print('-----'*6)
    print(' UL GRANT APPLICATION SYSTEM')
    print('-----'*6)
    print("\nA. Input Application Details for a Student. \
            \nB. Display Summary of Applications. \
            \nC. Display Grant Awardees. \
            \nX. Exit")

    while True:
        option = input().lower()

        # Validates menu options by checking if
        # the option input matches a list of stored options
        while option not in ['a', 'b', 'c', 'x']:
            print("Error!! Only option A, B, C & X are valid. Please try again:")
            break
        else:
            break

    if option == 'a':
        get_student_details()
    elif option == 'b':
        display_student_details()

def select_menu_opt():
    """Lets the usere select weather to process another application,
       go back to the main menu or exit the program.
    """

    print("Do you wish to enter another application? [y/n]: ")
    opt = input()
    if opt == 'n':
        print('Return to the main menu? [y/n]')
        opt = input()
        if opt == 'n':
            exit()
        else:
            menu()
    else:
        get_student_details()


def get_student_details():
    """Lets the user enter a set amount of student applications.
    """
    
    sp.call('clear', shell=True)
    print('\n')
    print('------'*5)
    print(" Student Application Details")
    print('------'*5)
    appMax = 0
    appMax += 1

    while appMax <= 25:
        uID = 1000
        uID = uID + 1
        ulID = 'UL'+str(uID)
        stuName = input("Enter students full name: ")
        stuGPA = input("Enter Student GPA: ")
        shortfal = input("Enter tuition shortfall: ")

        store_students(ulID, stuName, stuGPA, shortfal)

        
        select_menu_opt()


def store_students(id, name, gpa, shortfal):
    """Stores the instance of a student along 
    with a generated appliation ID to a data structure
    
    Arguments:
        name {sting} -- Students full name
        gpa {float} -- Students grade point average
        shortfal {float} -- Students tuition shortfall
    """
    student_data['student'] = []
    student_data['student'].append({
        'student id': id,
        'student name': name,
        'gpa': gpa,
        'shortfall' : shortfal
    })

    with open('student_data.json', 'w') as file:
        json.dump(student_data, file)

def display_student_details():
    with open('student_data.json', 'r') as jfile:
        student_data = json.load(jfile)
        for i in student_data['student']:
            print('Student ID: '+ i['student id'])
            print('Student Name: {}'.format(i['student name'].capitalize()))
            print('GPA: '+ i['gpa'])
            print('Shortfall: '+ i['shortfall'])




if __name__ == "__main__":
    menu()
